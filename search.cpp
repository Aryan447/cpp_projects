#include<iostream>
using namespace std;

int main()
{
        int arr[10], i, num, n, cnt=0, pos;
        cout<<"Enter Array Size : ";
        cin>>n;
        cout<<"Enter Array Elements: \n";
        for(i=0; i<n; i++)
        {
                cout<<" ";
                cin>>arr[i];
        }
        cout<<"\nEnter Element to be Searched : ";
        cin>>num;
        for(i=0; i<n; i++)
        {
                if(arr[i]==num)
                {
                        cnt=1;
                        pos=i+1;
                        break;
                }
        }
        if(cnt==0)
        {
                cout<<"\nElement Not Found..!!";
        }
        else
        {
                cout<<"\nElement "<<num<<" Found At Position "<<pos<<endl;
        }
        return 0;
}