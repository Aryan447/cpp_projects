#include<iostream>
using namespace std;

int main(){
    int a=5;
    int* b;
    b = &a;
    b = new int;  // new allocates the memory to program
    cout<<a<<endl;
    cout<<b<<endl;
    delete b;  // delete deallocates/free the memory of program to avoid crash

    return 0;
}