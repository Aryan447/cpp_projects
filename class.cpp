#include <iostream>
using namespace std;

    class car{
        public:  // access specifier
        string brand;
    };

int main(){
    car car1;  // car1 is object of class car
    car1.brand="BMW";
    cout<<car1.brand<<endl;
    return 0;
}
