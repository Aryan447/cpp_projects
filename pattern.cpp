#include <iostream>
using namespace std;

int main()
{
    int row, i, j;
    cout << "Enter number of rows: ";
    cin >> row;
    for (i = 1; i <= row; i++)
    {
        for (j = i; j < row; j++)
        {
            cout << " ";
        }
        for (j = 1; j <= (2 * i - 1); j++)
        {
            cout << "*";
        }
        cout << endl;
    }
    return 0;
}