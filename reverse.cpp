#include <iostream>
using namespace std;

int main(){
    int num,reverse, rem;
    reverse = 0;
    cout<<"enter number: ";
    cin>>num;
    while (num != 0) {
        rem = num % 10;
        reverse = reverse * 10 + rem;
        num /= 10;
    }
    cout<<"reversed number: "<<reverse<<endl;
    return 0;
}