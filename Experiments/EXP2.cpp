#include <iostream>
using namespace std;

class myclass
{
    private:
    int num;
    public:
    // function to assign value
    void data(int v)
    {
        num = v;
    }
    // function to print value
    void display()
    {
        cout << "num: " << num << endl;
    }
};

int main()
{
    // create object
    myclass a;
    a.data(100);
    a.display();

    return 0;
}
